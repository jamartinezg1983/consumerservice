
package com.jamartinez.microservices.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class ConsumerServiceApplication {

	public ConsumerServiceApplication(){
        //For Spring
    }
	
	public static void main(String[] args) {
        SpringApplication.run(ConsumerServiceApplication.class, args);
    }
}
